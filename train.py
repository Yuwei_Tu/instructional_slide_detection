import os
import numpy as np
import string
import random
from random import shuffle
import sklearn
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit, KFold, GridSearchCV
import sklearn.metrics as metrics
from sklearn.metrics import roc_auc_score, auc
import time
from collections import defaultdict
import pickle
import json
import pandas as pd
from collections import Counter
from nltk.stem import PorterStemmer
ps = PorterStemmer()
from pattern.text.en import singularize
import enchant
ENG_dict = enchant.Dict("en_US")



class DataWorker(object):

    SENTENCE_BREAK = ",.:;?!(){}[]"

    def __init__(self):
        self.min_phrase_length = 1
        self.max_phrase_length = 1
        self.stopwords = set(open('static/cta_stoplist.txt', 'r').readlines())
        self.stopwords = [word[:-1] for word in self.stopwords]

        self.exclude = set(string.punctuation)
        # self.exclude.discard('-')

        self.course_data = pickle.load(open("course_data.pickle", "rb"))
        self.course_data_labels = pickle.load(open("course_data_labels_1546464734.pickle", "rb"))
        self.flag_words = open('flag_words.txt', 'r').readlines()
        self.flag_words = [word[:-1] for word in self.flag_words]

        self.course_features = self.course_level_features()


    def course_level_features(self):

        d = {}
        for coursename in self.course_data_labels.keys():
            num_slides = len(self.course_data[coursename])
            num_words = sum([len(self.course_data[coursename][i].split()) for i in self.course_data[coursename]])
            avg_words_per_slide = num_words/float(num_slides)
            d[coursename] = [num_slides, num_words, avg_words_per_slide]

        return d


    def filter_string(self, s):
        """
        Filter and massage a string from a single source
        :param s: a string containing the raw text from one source
        :type s: str
        :return: str: the filtered string
        """
        # replace all non-ASCII characters with spaces
        s = ''.join([i if ord(i) < 128 else ' ' for i in s])
        # replace punctuation with space
        s = ''.join([i if i not in self.exclude else ' ' for i in s])
        # lowercase string
        s = s.lower()

        # remove any invalid english words
        words = s.split()
        #words = [ps.stem(word) for word in words if ENG_dict.check(word)]
        words = [word for word in words if not(True in [c.isdigit() for c in word]) ]
        words = [word for word in words if ENG_dict.check(word)]
        s = ' '.join(words)

        return s


    def prepare_bag_of_ngrams(self, s):
        """
        Filter and massage a string from a single source
        :param s: a string containing the raw text from one source
        :type s: str
        :return: str: the filtered string
        """
        # replace all non-ASCII characters with spaces
        s = self.filter_string(s)
        ngrams = [self.extract_phrases(s, l) for l in range(self.min_phrase_length, self.max_phrase_length + 1)]
        ngrams = [item for sublist in ngrams for item in sublist]
        ngrams = ' '.join(ngrams)
        return ngrams


    def extract_phrases(self, s, phrase_length):
        """
        Extract all valid phrases from string s of length phrase_length
        :param s: a sentence
        :type: docstrings
        :param phrase_length: length of the phrase to extract
        :type: int
        :return: a list of phrases of underline-separated words
        """
        sentences = []
        sentence = ''
        for char in s:
            if char in self.SENTENCE_BREAK:
                sentences.append(sentence)
                sentence = ''
            else:
                sentence += char
        if sentence:
            sentences.append(sentence)

        phrases = []
        for se in sentences:
            words_list = se.split()
            phrases += [self.validate_phrase(words_list[i: i + phrase_length]) for i in
                        range(len(words_list) - phrase_length + 1)]

        return [p for p in phrases if p]


    def validate_phrase(self, words):
        """
        Returns words joined by an underline if they constitute a valid phrase,
        otherwise an empty string

        :param words: a list containing one or more words
        :type words: string
        :return: string
        """
        for word in words:
            if word in self.stopwords:
                return ''
            # check if a word contains punctuation
            # if (True in [c in self.exclude_characters for c in word]):
            #     return ''
            if len(word) <= 1:
                return ''
            if not ENG_dict.check(word):
                return ''

        # if last word in phrase is plural, change to singular
        words[-1] = singularize(words[-1]) if ENG_dict.check(singularize(words[-1])) else words[-1]
        phrase = '_'.join(words)
        if phrase in self.stopwords:
            return ''

        return phrase



    def parse_features(self, slide_texts):

        # tf-idf
        vectorizer = TfidfVectorizer(stop_words=self.stopwords)

        X = vectorizer.fit_transform([self.filter_string(text) for text in slide_texts]).toarray()
        #X = vectorizer.fit_transform([self.prepare_bag_of_ngrams(text) for text in slide_texts]).toarray()

        # number of words, number of sentences (period), number of question marks
        additional = []
        for i, text in enumerate(slide_texts):
            text_length = len(text.split(' '))
            num_question_marks = sum([1 if char == '?' else 0 for char in text])
            num_period = sum([1 if char == '.' else 0 for char in text])
            num_comma = sum([1 if char == ',' else 0 for char in text])
            num_stopwords = sum([1 if w in self.stopwords else 0 for w in text.split(' ')])

            stopwords_fraction = num_stopwords / float(text_length)
            punctuation_fraction = (num_comma + num_period + num_question_marks)/float(text_length)
            additional.append([text_length, num_question_marks, num_period, num_comma, num_stopwords,
                               stopwords_fraction, punctuation_fraction])

            for i, word in enumerate(self.flag_words):
                num_word = sum([1 if w == word else 0 for w in text.split(' ')])
                additional.append(num_word)

        additional = np.asarray(additional)
        X = np.concatenate((X, additional), axis=1)

        return X


    def eval(self, y_actual, y_pred):
        fpr, tpr, thresholds = metrics.roc_curve(y_actual, y_pred)
        # print 'auc: ', metrics.auc(fpr, tpr)
        # print 'roc_auc_score: ', roc_auc_score(y_actual, y_pred)
        return metrics.auc(fpr, tpr)




    def data_prep(self, training_course_names, testing_course_names):

        XY_training = []
        for c in training_course_names:
            for ind in self.course_data_labels[c]:
                XY_training.append( [ self.course_data[c][ind], self.course_data_labels[c][ind], self.course_features[c] ] )
        X_training_texts = [item[0] for item in XY_training]
        Y_train = [1 if item[1] == 'y' else 0 for item in XY_training]
        X_training_course_features = [item[2] for item in XY_training]


        XY_testing = []
        for c in testing_course_names:
            for ind in self.course_data_labels[c]:
                XY_testing.append([self.course_data[c][ind], self.course_data_labels[c][ind], self.course_features[c] ])
        X_testing_texts = [item[0] for item in XY_testing]
        Y_test = [1 if item[1] == 'y' else 0 for item in XY_testing]
        X_testing_course_features = [item[2] for item in XY_testing]


        self.additional_features = ['text_length', 'num_question_marks', 'num_period', 'num_comma', 'num_stopwords',
                                   'stopwords_fraction', 'punctuation_fraction']

        def get_additional_features(texts, course_features):

            # number of words, number of sentences (period), number of question marks
            additional = []
            for i, text in enumerate(texts):
                text_length = len(text.split(' '))
                num_question_marks = sum([1 if char == '?' else 0 for char in text])
                num_period = sum([1 if char == '.' else 0 for char in text])
                num_comma = sum([1 if char == ',' else 0 for char in text])
                num_stopwords = sum([1 if w in self.stopwords else 0 for w in text.split(' ')])

                stopwords_fraction = num_stopwords / float(text_length)
                punctuation_fraction = (num_comma + num_period + num_question_marks)/float(text_length)

                tmp = [text_length, num_question_marks, num_period, num_comma, num_stopwords,
                                   stopwords_fraction, punctuation_fraction]


                words = text.split(' ')
                for _, word in enumerate(self.flag_words):
                    num_word = sum([1 if w == word else 0 for w in words])
                    tmp.append(num_word)


                # tmp += course_features[i]
                # tmp.append( text_length / float(course_features[i][2]) )

                additional.append(tmp)

            return np.asarray(additional)

        # vectorizer = TfidfVectorizer(stop_words='english')
        # tmp = vectorizer.fit_transform([self.filter_string(text) for text in X_training_texts]).toarray()
        # X_training = np.concatenate((tmp, get_additional_features(X_training_texts)), axis=1)
        #
        # tmp = vectorizer.transform([self.filter_string(text) for text in X_testing_texts]).toarray()
        # X_testing = np.concatenate((tmp, get_additional_features(X_testing_texts)), axis=1)
        #
        # self.tfidf_feature_names = vectorizer.get_feature_names()
        # # self.vocabulary = vectorizer.vocabulary_

        X_training = get_additional_features(X_training_texts, X_training_course_features)
        X_testing = get_additional_features(X_testing_texts, X_testing_course_features)

        return X_training, X_testing, Y_train, Y_test


    def train_get_flag_words(self):

        course_names = self.course_data_labels.keys()
        kFold = KFold(n_splits=5, shuffle=True, random_state=None)


        tops = []
        for train_index, test_index in kFold.split(self.course_data_labels.keys()):
            training_course_names = [course_names[i] for i in train_index]
            testing_course_names = [course_names[i] for i in test_index]

            X_train, X_test, y_train, y_test = self.data_prep(training_course_names, testing_course_names)

            tmp, auc = self.train_once(X_train, X_test, y_train, y_test)
            tops += tmp
            print 'auc: ', auc

        d = defaultdict(list)
        for t in tops:
            d[t[0]] += [t[1]]
        for feature in d:
            d[feature] = np.average(d[feature])

        sorted_by_value = sorted(d.items(), key=lambda kv: kv[1], reverse=True)
        for item in sorted_by_value:
            print item

        with open('flag_words.txt', 'w') as f:
            for item in sorted_by_value:
                f.write("%s" %item[0])

        return 0


    def parameter_tuning_GBM(self):
        course_names = self.course_data_labels.keys()
        kFold = KFold(n_splits=5, shuffle=False, random_state=None)

        data = []
        for train_index, test_index in kFold.split(self.course_data_labels.keys()):
            training_course_names = [course_names[i] for i in train_index]
            testing_course_names = [course_names[i] for i in test_index]
            X_train, X_test, y_train, y_test = self.data_prep(training_course_names, testing_course_names)
            data.append((X_train, X_test, y_train, y_test))

        learning_rates = [1, 0.25, 0.05]
        n_estimators = [25, 50, 100, 200]
        max_depths = [5, 10, 20]
        subsamples = [0.5, 0.8, 1.0]
        min_samples_split = [2, 10, 100, 200]
        min_samples_leaf = [1, 10, 100]
        # todo:  [1, 50, 5, 0.8, 100, 10, 0.8138461538461539]   <----
        #  1 100 10 0.8 100 1 0.839230769231    without course level features
        # [1, 100, 5, 0.5, 200, 1, 0.868076923076923]   <----    without course level features

        # todo:  grow a tree to max depth and and recursively prune all the leaf splits with negative gain!!

        all = []
        for eta in learning_rates:
            for n_est in n_estimators:
                for max_dep in max_depths:
                    for subsample in subsamples:
                        for split in min_samples_split:
                            for leaf in min_samples_leaf:
                                auc_GBMs = []
                                for X_train, X_test, y_train, y_test in data:
                                    clf = GradientBoostingClassifier(learning_rate=eta,
                                                                     n_estimators=n_est,
                                                                     max_depth=max_dep,
                                                                     subsample=subsample,
                                                                     min_samples_split=split,
                                                                     min_samples_leaf=leaf)

                                    clf.fit(X_train, y_train)
                                    y_pred = clf.predict(X_test)
                                    auc_GBM = self.eval(y_test, y_pred)
                                    auc_GBMs.append(auc_GBM)

                                    print  eta, n_est, max_dep, subsample, split, leaf, np.average(auc_GBMs)
                                    all.append( [eta, n_est, max_dep, subsample, split, leaf, np.average(auc_GBMs)] )
                                    # # 1 50 5 0.8 100 10 0.813846153846


        try:
            pd.DataFrame(all).to_csv('results_%s.csv'%str(int(time.time())))
        except:
            print 'failed to save as .csv'

        max_auc = max([item[-1] for item in all])
        ans = []
        for item in all:
            if item[-1] == max_auc:
                ans += [item]
                print item, '  <---- '
            else:
                pass
                # print item


    def parameter_tuning_LR(self):

        course_names = self.course_data_labels.keys()
        kFold = KFold(n_splits=5, shuffle=False, random_state=None)

        data = []
        for train_index, test_index in kFold.split(self.course_data_labels.keys()):
            training_course_names = [course_names[i] for i in train_index]
            testing_course_names = [course_names[i] for i in test_index]
            X_train, X_test, y_train, y_test = self.data_prep(training_course_names, testing_course_names)
            data.append((X_train, X_test, y_train, y_test))

        penalty = [('l2', 'lbfgs'), ('l1', 'liblinear')]
        Cs = [0.1,0.5,1,5,10,20,40,80,100,200,300,400,500,700,900,1000]

        all = []
        for p, solver in penalty:
            for c in Cs:
                auc_scores = []
                for X_train, X_test, y_train, y_test in data:
                    lr = LogisticRegression(penalty=p, C=c,
                                            solver=solver).fit(X_train, y_train)
                    y_pred = lr.predict(X_test)
                    auc_LR = self.eval(y_test, y_pred)

                    auc_scores.append(auc_LR)

                print  p, c, np.average(auc_scores)
                all.append( [p, c, np.average(auc_scores)] )


        try:
            pd.DataFrame(all, columns=['penalty', 'C', 'auc']).to_csv('LR_results_%s.csv'%str(int(time.time())))
        except:
            print 'failed to save as .csv'

        max_auc = max([item[-1] for item in all])
        ans = []
        for item in all:
            if item[-1] == max_auc:
                ans += [item]
                print item, '  <---- '



    def main(self):

        course_names = self.course_data_labels.keys()
        kFold = KFold(n_splits=5, shuffle=False, random_state=None)

        auc_GaussianNBs, auc_BernoulliNBs, auc_LRs, auc_GBMs = [], [], [], []

        for train_index, test_index in kFold.split(self.course_data_labels.keys()):
            training_course_names = [course_names[i] for i in train_index]
            testing_course_names = [course_names[i] for i in test_index]
            X_train, X_test, y_train, y_test = self.data_prep(training_course_names, testing_course_names)
            _, auc_GaussianNB, auc_BernoulliNB, auc_LR, auc_GBM = self.train_once(X_train, X_test, y_train, y_test)
            auc_GaussianNBs.append(auc_GaussianNB)
            auc_BernoulliNBs.append(auc_BernoulliNB)
            auc_LRs.append(auc_LR)
            auc_GBMs.append(auc_GBM)

        print np.average(auc_GaussianNBs)
        print np.average(auc_BernoulliNBs)
        print np.average(auc_LRs)
        print np.average(auc_GBMs)

        return 0


    def train_once(self, X_train, X_test, y_train, y_test):

        print 'Training: ', X_train.shape, 'Testing: ', X_test.shape

        nb = GaussianNB()
        nb.fit(X_train, y_train)
        y_pred = nb.predict(X_test)
        auc_GaussianNB = self.eval(y_test, y_pred)

        nb = BernoulliNB()
        nb.fit(X_train, y_train)
        y_pred = nb.predict(X_test)
        auc_BernoulliNB = self.eval(y_test, y_pred)

        lr = LogisticRegression(random_state=0, solver='lbfgs', multi_class = 'multinomial').fit(X_train, y_train)
        y_pred = lr.predict(X_test)
        auc_LR = self.eval(y_test, y_pred)

        clf = GradientBoostingClassifier(n_estimators=200)
        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)
        auc_GBM = self.eval(y_test, y_pred)

        imp = clf.feature_importances_
        sorted_indexes = np.argsort(imp)[::-1]

        top_features = []
        # for i in xrange(200):
        #     if sorted_indexes[i] < len(self.tfidf_feature_names):
        #         #print self.tfidf_feature_names[sorted_indexes[i]],  imp[sorted_indexes[i]]
        #         top_features += [ (self.tfidf_feature_names[sorted_indexes[i]],  imp[sorted_indexes[i]]) ]
        #     else:
        #         tmp = sorted_indexes[i] - len(self.tfidf_feature_names)
        #         #print self.additional_features[tmp], imp[tmp]
        #         top_features += [ (self.additional_features[tmp], imp[tmp]) ]

        # print '------------------------\n\n'


        print 'GaussianNB: ', auc_GaussianNB
        print 'BernoulliNB: ', auc_BernoulliNB
        print 'Logistic Regression: ', auc_LR
        print 'Gradient Boosting: ', auc_GBM
        print '------------------------\n\n'


        # print 'Random Forest: '
        # clf = RandomForestClassifier(n_estimators=200, max_depth=10)
        # clf.fit(X_train, y_train)
        # y_pred = clf.predict(X_test)
        # self.eval(y_test, y_pred)

        # print 'SVM: '
        # svm_clf = SVC(kernel='rbf',gamma='auto')
        # svm_clf.fit(X_train, y_train)
        # y_pred = svm_clf.predict(X_test)
        # self.eval(y_test, y_pred)

        return top_features, auc_GaussianNB, auc_BernoulliNB, auc_LR, auc_GBM




if __name__ == '__main__':

    # course_data = pickle.load(open("course_data.pickle", "rb"))
    # course_data_labels = pickle.load(open("course_data_labels_1546464734.pickle", "rb"))
    #
    # instructional_slides = []
    # count = 0
    # for course in course_data:
    #     print course
    #     if course not in course_data_labels:
    #         continue
    #     for slide in course_data[course]:
    #         if course_data_labels[course][slide] == 'y':
    #             instructional_slides.append( course_data[course][slide] )
    #
    #
    # with open('instructional_slides.json', 'wb') as outfile:
    #     json.dump(instructional_slides, outfile)


    Worker = DataWorker()
    # print Counter(Worder.Y)   # Counter({'n': 1548, 'y': 171})

    # Worker.train_get_flag_words()
    # Worker.main()
    # Worker.parameter_tuning_GBM()
    Worker.parameter_tuning_LR()