


sample1 = "Many problems in L. C.   M. F.   deal with taking expectations of functions under probability distributions. From " \
         "the Law of Large Numbers, an expectation can be efficiently approximated by a M.C. estimator. Therefore, " \
         "if we can draw samples from a distribution (even if we don't know the distribution itself) then we can " \
         "compute a Monte Carlo estimate of the expectation in question. The key is that we don't need to have an " \
         "expression for the distribution: If we just have LCM.F. then we can compute the expectations that we are " \
         "interested in.  Ideally, if the first draw was in an area of high probability mass then the second draw is " \
         "also likely to be accepted. Therefore, you end up accepting many more samples and you don't waste time " \
         "drawing samples that are to be REJECTED. The amazing thing is that if you run the Markov Chain long " \
         "enough (i.e. to infinity) and under specific conditions (the chain must be finite, aperiodic, irreducible " \
         "and ergodic) then your samples will be drawn from the true posterior of your model. That's amazing! " \
         "The M.C.M.C. technique is to draw dependent samples so it scales to a higher dimensionality than previous" \
         " methods, but under the right conditions, even though the samples are dependent, they are as if " \
         "they are drawn IID from the desired distribution (which is the posterior in Bayesian Inference)."

sample2 = 'Suppose the end of a setence is L. C.  M. F. Then how do you avoid changing the second sentence? '
sample3 = 'Suppose the end of a setence is L. C.  M. F.   LCMF is the start of the second stentence. how do you avoid changing the second sentence? '


sample4 = 'Many problems in L. C.   M. F.   deal with taking expectations of functions under probability distributions.'

# patterns:
#   A.
#   AB.
#   ABC.
#   A
#   AB
#   ABC




def acronyms_consolidation(text):

    def check(x):
        if text[x].isupper() or text[x] == '.' or text[x] == ' ':
            return check(x+1)
        return x

    new, i, l = '', 0, len(text)

    while i < l:

        if text[i].isupper() and (text[i+1].isupper() or text[i+1] == '.'):
            j = check(i)
            indices_of_period = [tmp for tmp in xrange(i, j) if text[tmp] == '.']

            if len(indices_of_period) <= 1:
                new += text[i:j]
            else:
                items = [item.strip() for item in  text[i:j].split('.')]

                if len(items[-1]) >= 1 and items[-1][0].isupper():
                    # the segment text[i:j] is the pattern "A.B.C." followed by the first word of next sentence.
                    j = indices_of_period[-1] + 1
                    new += ''.join([char for char in text[i:j] if char is not '.' and char is not ' ']) + '.'

                else:  # the segment text[i:j] looks like the patterm "A.B.C."
                    new += ''.join([char for char in text[i:j] if char is not '.' and char is not ' ']) + ' '


            i = j

        else:
            new += text[i]
            i += 1

    return new



sample = sample1

for sample in [sample1, sample2, sample3, sample4]:
    print sample
    print acronyms_consolidation(text=sample)
    print '\n'
